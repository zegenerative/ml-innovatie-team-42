import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css';
import * as tf from '@tensorflow/tfjs';
import * as tfvis from '@tensorflow/tfjs-vis';

// Semantic
import { Input, Loader, Button, Container, Segment } from 'semantic-ui-react';

function plotGraph(points, name) {
  tfvis.render.scatterplot(
    {
      name: `${name} vs price`,
    },
    {
      values: [points],
      series: ['Original data'],
    },
    {
      xLabel: name,
      yLabel: 'price',
    }
  );
}

function App() {
  const [data, setData] = React.useState();

  async function fetchData() {
    const csv = tf.data.csv('http://localhost:3000/kc_house_data.csv');
    const dataSet = csv.map((el) => ({
      y: el['price'],
      x: el['sqft_living'],
    }));
    const dataSetArray = await dataSet.toArray();

    setData(dataSetArray);
  }

  React.useEffect(() => {
    fetchData();
  }, []);

  if (!data) return <Loader />;

  plotGraph(data, 'Sqft living');

  return (
    <div className="App">
      <Container textAlign="center" style={{ marginTop: 200 }}>
        <Segment>
          <Button>Train Model</Button>
          <Segment>
            <Input action="Predict" />
          </Segment>
        </Segment>
      </Container>
    </div>
  );
}

export default App;
